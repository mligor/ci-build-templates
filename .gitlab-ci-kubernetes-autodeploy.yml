variables:
  # Custom configuration (required)
  # BX_URL_PATH: /reporting
  # BX_SERVICE_NAME: timeorg-reporting
  # BX_SERVICE_PORT: 8080
  # BX_URL_DOMAIN: timeorg.pro
  # BX_NAMESPACE: timeorg

  # For Kubernetes executors, we use tcp://docker:2376
  DOCKER_HOST: tcp://localhost:2375
  # This will instruct Docker not to start over TLS.
  DOCKER_TLS_CERTDIR: ""
  DOCKER_DRIVER: overlay2

  BUILD_IMAGE_NAME: "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG"
  UNIQUE_IMAGE_NAME: "$CI_REGISTRY_IMAGE:$CI_COMMIT_SHA"
  RELEASE_IMAGE_NAME: "$CI_REGISTRY_IMAGE:stable"
  LATEST_IMAGE_NAME: "$CI_REGISTRY_IMAGE:latest"
  STAGING_IMAGE_NAME: "$CI_REGISTRY_IMAGE:staging"
  KUBE_NAMESPACE: "$BX_NAMESPACE"
  TLS_SECRET_NAME: default-tls

stages:
  - build
  - staging
  - production

default:
  image: mligor/docker-kubectl-helm
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
    - helm repo add ci-build-templates https://gitlab.com/mligor/ci-build-templates/raw/master/
  services:
    - docker:stable-dind

build:
  stage: build
  script:
    - docker build --cache-from "$LATEST_IMAGE_NAME" --tag "$BUILD_IMAGE_NAME" --tag "$UNIQUE_IMAGE_NAME" --tag "$LATEST_IMAGE_NAME" .
    - docker push "$BUILD_IMAGE_NAME"
    - docker push "$UNIQUE_IMAGE_NAME"
    - docker push "$LATEST_IMAGE_NAME"

staging:
  stage: staging
  environment:
    name: staging
    url: https://staging.$BX_URL_DOMAIN$BX_URL_PATH
    on_stop: stop_staging
  script:
    - export BX_NAMESPACE=$BX_NAMESPACE-staging
    - export BX_URL_DOMAIN=staging.$BX_URL_DOMAIN
    - |
      helm upgrade $BX_SERVICE_NAME ci-build-templates/kubernetes-autodeploy-chart --install \
        --namespace $BX_NAMESPACE \
        --set imageCredentials.registry=$CI_REGISTRY  \
        --set imageCredentials.username=$CI_DEPLOY_USER  \
        --set imageCredentials.password=$CI_DEPLOY_PASSWORD \
        --set image.repository=$UNIQUE_IMAGE_NAME  \
        --set treafik.host=$BX_URL_DOMAIN  \
        --set treafik.path=$BX_URL_PATH  \
        --set service.port=$BX_SERVICE_PORT \
        --set fullnameOverride=$BX_SERVICE_NAME \
        --set traefik.tlsSecretName=$TLS_SECRET_NAME
    - docker pull "$UNIQUE_IMAGE_NAME"
    - docker tag "$UNIQUE_IMAGE_NAME" "$STAGING_IMAGE_NAME"
    - docker push "$STAGING_IMAGE_NAME"

stop_staging:
  stage: staging
  environment:
    name: staging
    action: stop
  variables:
    GIT_STRATEGY: none
  when: manual
  script:
    - export BX_NAMESPACE=$BX_NAMESPACE-staging
    - export BX_URL_DOMAIN=staging.$BX_URL_DOMAIN
    - helm uninstall $BX_SERVICE_NAME  --namespace $BX_NAMESPACE

production:
  stage: production
  environment:
    name: production
    url: https://$BX_URL_DOMAIN$BX_URL_PATH
    on_stop: stop_production
  when: manual
  script:
    - |
      helm upgrade $BX_SERVICE_NAME ci-build-templates/kubernetes-autodeploy-chart --install \
        --namespace $BX_NAMESPACE \
        --set imageCredentials.registry=$CI_REGISTRY  \
        --set imageCredentials.username=$CI_DEPLOY_USER  \
        --set imageCredentials.password=$CI_DEPLOY_PASSWORD \
        --set image.repository=$UNIQUE_IMAGE_NAME  \
        --set treafik.host=$BX_URL_DOMAIN  \
        --set treafik.path=$BX_URL_PATH  \
        --set service.port=$BX_SERVICE_PORT \
        --set fullnameOverride=$BX_SERVICE_NAME \
        --set traefik.tlsSecretName=$TLS_SECRET_NAME
    - docker pull "$UNIQUE_IMAGE_NAME"
    - docker tag "$UNIQUE_IMAGE_NAME" "$RELEASE_IMAGE_NAME"
    - docker push "$RELEASE_IMAGE_NAME"

stop_production:
  stage: production
  environment:
    name: production
    action: stop
  variables:
    GIT_STRATEGY: none
  when: manual
  script:
    - helm uninstall $BX_SERVICE_NAME  --namespace $BX_NAMESPACE
